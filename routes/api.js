var express = require('express');
var router = express.Router();

// helper file
var config = require('../helpers/config');
var db = require('../helpers/db');
var common = require('../helpers/common');
var error_logger = require('../helpers/error_logger');
var {arrayEmail,stringOrArrayEmail} = require('../helpers/custom_validator');

// external plugins
var uuid = require('uuid/v4');
var { validations,validate,validateAll,is } = require('indicative');
validations.arrayEmail = arrayEmail;
validations.stringOrArrayEmail = stringOrArrayEmail;

/*
		Route: {Domain}/api/register
		Register one or more students to a specified teacher
*/
router.post('/register', (req, res, next) => {
		
		// parameter validation
    	validateAll(req.body, {
	      	teacher:'required|email',
	      	students:'arrayEmail'
	    })
	    .then(async ()=>{
	  		try
	  		{
	  			var param=req.body;
		    	var teacher_id;
		    	var student_id;
		    	var students_ids=[];

		    	// start database transaction
		    	await db.query('Start transaction');

		    	// find or create teacher user 
		    	var query=await db.query('select * from user where email=?',[param.teacher]);
		    	if(query.length==0)
		    	{
		    		teacher_id=uuid();
		    		var query=await db.query('insert into user set ?',{
		    			user_id:teacher_id,
		    			role_id:config.role_teacher,
		    			email:param.teacher,
		    			active:'y',
		    			created_at:common.getCurrentDateTime(),
		    			updated_at:common.getCurrentDateTime()
		    		});
		    	}
		    	else
		    	{
		    		teacher_id=query[0].user_id;
		    	}

		    	// find or create student user
		    	for(var i=0;i<param.students.length;i++)
				{
					var query=await db.query('select * from user where email=?',[param.students[i]]);

					if(query.length==0)
			    	{
			    		student_id=uuid();
			    		var query=await db.query('insert into user set ?',{
			    			user_id:student_id,
			    			role_id:config.role_student,
			    			email:param.students[i],
			    			active:'y',
			    			created_at:common.getCurrentDateTime(),
			    			updated_at:common.getCurrentDateTime()
			    		});

			    	}
			    	else
			    	{
			    		student_id=query[0].user_id;
			    	}

			    	students_ids.push(student_id);
				}

				// find of create registration
				for(let i=0;i<students_ids.length;i++)
				{
					var query=await db.query('select * from registration where teacher_id=? and student_id=?',[teacher_id,students_ids[i]]);

					if(query.length==0)
			    	{
			    		var query=await db.query('insert into registration set ?',{
			    			teacher_id:teacher_id,
			    			student_id:students_ids[i],
			    			created_at:common.getCurrentDateTime()
			    		});

			    	}
				}

				await db.query('commit');

				res.status(204).send();
	  		}
	  		catch(err)
	  		{ 
	  			// exception handler
	  			next(err); 
	  		}
	    })
	    .catch((err)=>{
	    	next({error_code:400,error_message: 'Invalid HTTP parameter'});
	    })

});

/*
		Route: {Domain}/api/commonstudents
		Retrieve a list of students common to a given list of teachers
*/
router.get('/commonstudents', (req, res, next) => {

  		// parameter validation
    	validateAll(req.query, {
	      	teacher:'stringOrArrayEmail'
	    })
	    .then(async ()=>{
	  		try{
	  			var param=req.query;
		    	var teachers=[];
				var output={students:[]};

				// convert param into array form
				if(Array.isArray(param.teacher))
				{
					teachers=param.teacher;
				}
				else
				{
					teachers.push(param.teacher);
				}

				// construct response data in specific format
				var query=await db.query('select distinct c.email from registration a join user b on a.teacher_id=b.user_id join user c on a.student_id=c.user_id where b.role_id=? and b.email in(?)',[config.role_teacher,teachers]);
				output.students=query.map(x=>x.email);
				
				res.status(200).json(output);
	  		}
	  		catch(err)
	  		{ 
	  			// exception handler
	  			next(err); 
	  		}
	    })
	    .catch((err)=>{
	    	next({error_code:400,error_message: 'Invalid HTTP parameter'});
	    })

});

/*
		Route: {Domain}/api/suspend
		Suspend a specified student
*/
router.post('/suspend', (req, res, next) => {

  		// parameter validation
    	validateAll(req.body, {
	      	student:'required|email'
	    })
	    .then(async ()=>{
	  		try{
				var param=req.body;
				var student;
				
				// find the target student and update active to 'n'
				var query=await db.query('select * from user where role_id=? and email=?',[config.role_student,param.student]);
				if(query.length!=0)
				{
					await db.query("update user set active='n' where role_id=? and user_id=?",[config.role_student,query[0].user_id]);
					res.status(204).send();
				}
				else
				{
					next({error_code:400,error_message: 'Student does not exist'});
				}
	  		}
	  		catch(err)
	  		{ 
	  			// exception handler
	  			next(err); 
	  		}
	    })
	    .catch((err)=>{
	    	next({error_code:400,error_message: 'Invalid HTTP parameter'});
	    })

});

/*
	Route: {Domain}/api/retrievefornotifications
	Retrieve a list of students who can receive a given notification
*/
router.post('/retrievefornotifications', (req, res, next) => {

  		// parameter validation
    	validateAll(req.body, {
	      	teacher:'required|email',
	      	notification:'required'
	    })
	    .then(async ()=>{
	  		try{
				var param=req.body;
				var teacher;
				var student=[];
				var student_list=[];
				var notification;
				var output={recipients:[]};

				// get hashtag active student list
				notification=param.notification.split(' ');
				for(let i=0;i<notification.length;i++)
				{
					if(notification[i].charAt(0)=='@')
					{
						var query=await db.query("select * from user where email=? and role_id=? and active='y'",[notification[i].substr(1),config.role_student]);
						if(query.length!=0)
						{
							student.push(notification[i].substr(1))
						}
					}
				}

				// check whether teacher's email is valid
				var query=await db.query("select * from user where email=? and role_id=? and active='y'",[param.teacher,config.role_teacher]);

				if(query.length!=0)
				{
					// combine registered and hashtag active student and remove duplication
					var query=await db.query("select c.email from registration a join user b on a.teacher_id=b.user_id join user c on a.student_id=c.user_id where c.active='y' and b.email=?",[param.teacher]);
					student_list=(query.map(x=>x.email)).concat(student);
					output.recipients=student_list.filter((item, index)=>student_list.indexOf(item) >= index);

					res.status(200).json(output);
				}
				else
				{
					next({error_code:400,error_message: 'Teacher does not exis'});
				}
	  		}
	  		catch(err)
	  		{ 
	  			// exception handler
	  			next(err); 
	  		}
	    })
	    .catch((err)=>{
	    	next({error_code:400,error_message: 'Invalid HTTP parameter'});
	    })

});

module.exports = router;
