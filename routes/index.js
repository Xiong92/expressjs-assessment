var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', (req, res, next) => {
  res.json({ topic: 'Assessment for node.js skills' });
});

module.exports = router;
