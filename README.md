# NodeJS Assessment

This is the example project to assess proficiency of NodeJS skills

This project is developed by using following technology:
1. ExpressJS framework (https://expressjs.com/)
2. Mysql
3. Mocha.js + Chai.js

## Setup

1) Make sure the pc has install the latest version of node.js and mysql
2) Git clone or download the project source code
3) Run 'npm install' from project root path to grab all the dependencies
4) Create a mysql database named as 'assessment' 
5) Run the sql query from 'assessment.sql' file
6) Modify the helpers/config file database credential accordingly
7) Run 'npm start' command at the project root path
8) Project will run in localhost with 3333 port number 'http://localhost:3333'
9) Enjoy api testing

## Unit Testing

Mocha.js + Chai.js be choosen as the unit testing tools<br/>
Execute 'npm run test' in the root path to see the unit testing result 


## Project Structure Brief Explained

ExpressJS is a fast, unopinionated, minimalist web framework for Node.js

helpers<br/>
Config - Storing the project credential<br/>
Common - Storing the shareable code<br/>
custom_validator - Storing any custom validator rules<br/>
db - Mysql library instance<br/>
error_logger - Winston logger instance

routes<br/>
\- Define all the controller logic here

app.js<br/>
\- Define middleware logic here

error.log<br/>
\- Viewing system error log here

test<br/>
\- Storing all the unit test cases


## Remark

Response status code:<br/><br/>
200,204 - Success<br/>
400 - Bad request ( Handled error )<br/>
500 - Internal server error ( Unexpected error )<br/>

\* Any occur error message will automatic store in error log file for checking purpose next time




