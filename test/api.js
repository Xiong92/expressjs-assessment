var chai = require('chai');
var chaiHttp = require('chai-http');
var should = chai.should();
var db = require('../helpers/db');
var app = require('../app');
chai.use(chaiHttp);

describe('Api', () => {

  before(async (done) => {
      done();
  });

  describe('/POST register', () => {
      it('Register one or more students to a specified teacher', (done) => {
        chai.request(app)
            .post('/api/register')
            .send({
                teacher:'teacher1@gmail.com',
                students:['student1@gmail.com','student2@gmail.com']
            })
            .end((err, res) => {
              res.should.have.status(204);
              done();
            });
      });
  });

  describe('/GET commonstudents', () => {
      it('Retrieve a list of students common to a given list of teachers', (done) => {
        chai.request(app)
            .get('/api/commonstudents?teacher=teacher1@gmail.com')
            .end((err, res) => {
              res.should.have.status(200);
              res.body.should.be.a('object');
              res.body.should.have.property('students');
              res.body.students.should.be.a('array');
              done();
            });
      });
  });

  describe('/POST suspend', () => {
      it('Suspend a specified student', (done) => {
        chai.request(app)
            .post('/api/suspend')
            .send({
                student:'student1@gmail.com'
            })
            .end((err, res) => {
              res.should.have.status(204);
              done();
            });
      });
  });

  describe('/POST retrievefornotifications', () => {
      it('Retrieve a list of students who can receive a given notification', (done) => {
        chai.request(app)
            .post('/api/retrievefornotifications')
            .send({
                teacher:'teacher1@gmail.com',
                notification:'Hello student2@gmail.com'
            })
            .end((err, res) => {
              res.should.have.status(200);
              res.body.should.be.a('object');
              res.body.should.have.property('recipients');
              res.body.recipients.should.be.a('array');
              done();
            });
      });
  });

});