var moment = require('moment');

module.exports={
	
	getCurrentDateTime:()=>{
		return moment().format('YYYY-M-D H:m:s');
	}

};
