var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

// error logger
var error_logger = require('./helpers/error_logger');

// route
var indexRouter = require('./routes/index');
var apiRouter = require('./routes/api');

var app = express();

// view engine setup
// app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// route prefix
app.use('/', indexRouter);
app.use('/api', apiRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler

// custome error handler
app.use(function(err, req, res, next) {
	if(err.error_code==400)
	{
		error_logger.info(err.error_message);
		res.status(err.error_code).json({ message: err.error_message });
	}
	else
	{
		next(err);
	}
});

// default error handler
app.use(function(err, req, res, next) {
	error_logger.info(err.message);
	res.status(500).json({ message: 'Internal server error' });
});

// app.use(function(err, req, res, next) {
//   // set locals, only providing error in development
//   res.locals.message = err.message;
//   res.locals.error = req.app.get('env') === 'development' ? err : {};

//   // render the error page
//   res.status(err.status || 500);
//   res.render('error');
// });

module.exports = app;
